package main

import (
	"encoding/xml"
	"fmt"
	"net/http"
	"net/url"
	"os"
	"strings"

	link_parser "gitlab.com/go-study-projects/link-parser"
)

func main() {
	if err := run(os.Args); err != nil {
		fmt.Fprintf(os.Stderr, "%s\n", err)
		os.Exit(1)
	}
}

func run(args []string) error {
	if len(os.Args) == 1 {
		return fmt.Errorf("specify a link as the first argument")
	}
	uri := args[1]
	if uri[len(uri)-1] == '/' {
		uri = uri[:len(uri)-1]
	}

	baseDomain, _ := getBaseDomain(uri)
	sitemap := make(set)
	walk(baseDomain, uri, &sitemap, true)

	siteSlice := filter(map2slice(&sitemap), withPrefix(baseDomain))
	if err := encode2File(siteSlice); err != nil {
		return err
	}

	return nil
}

type set map[string]struct{}

const xmlns = "http://www.sitemaps.org/schemas/sitemap/0.9"

type urlset struct {
	Urls  []loc  `xml:"url"`
	Xmlns string `xml:"xmlns,attr"`
}

type loc struct {
	Value string `xml:"loc"`
}

// Recursively walk all links starting from the given uri. DFS approach.
func walk(baseDomain, uri string, sitemap *set, withinBaseDomain bool) {
	// Add url to the sitemap
	(*sitemap)[uri] = struct{}{}
	//fmt.Println(uri)

	if withinBaseDomain && !strings.HasPrefix(uri, baseDomain) {
		return
	}

	// GET the page
	response, err := http.Get(uri)
	if err != nil {
		return
	}
	defer response.Body.Close()

	// Do DFS
	links, _ := link_parser.Parse(response.Body)
	for _, link := range links {
		nextURI := getProperURI(baseDomain, link.Href)

		// The link is not interesting
		if nextURI == "" {
			continue
		}
		// Proceed if link is not already in the sitemap
		if _, ok := (*sitemap)[nextURI]; !ok {
			walk(baseDomain, nextURI, sitemap, true)
		}
	}
}

func getBaseDomain(uri string) (string, error) {
	response, err := http.Get(uri)
	if err != nil {
		return "", err
	}
	defer response.Body.Close()

	reqURL := response.Request.URL
	baseURL := &url.URL{
		Scheme: reqURL.Scheme,
		Host:   reqURL.Host,
	}

	return baseURL.String(), nil
}

// Prepend baseDomain if uri is not full.
// Return empty string if uri does not start with 'http' or '/'.
func getProperURI(baseDomain, uri string) string {
	switch {
	case strings.HasPrefix(uri, "http"):
		return uri
	case strings.HasPrefix(uri, "/"):
		return baseDomain + uri
	}
	return ""
}

func filter(links []string, keepFn func(string) bool) []string {
	var linksOut []string
	for _, link := range links {
		if keepFn(link) {
			linksOut = append(linksOut, link)
		}
	}
	return linksOut
}

func withPrefix(pfx string) func(string) bool {
	return func(uri string) bool {
		return strings.HasPrefix(uri, pfx)
	}
}

func map2slice(s *set) []string {
	var out []string
	for k := range *s {
		out = append(out, k)
	}
	return out
}

func encode2File(urls []string) error {
	file, err := os.Create("sitemap.xml")
	if err != nil {
		return fmt.Errorf("error creating XML file:", err)
	}
	defer file.Close()

	if err := writeHeader(file); err != nil {
		return err
	}

	toXml := urlset{
		Xmlns: xmlns,
	}
	for _, uri := range urls {
		toXml.Urls = append(toXml.Urls, loc{uri})
	}

	encoder := xml.NewEncoder(file)
	encoder.Indent("", "  ")

	if err := encoder.Encode(toXml); err != nil {
		return fmt.Errorf("error encoding XML:", err)
	}

	return nil
}

func writeHeader(file *os.File) error {
	_, err := file.Write([]byte(xml.Header))
	if err != nil {
		return fmt.Errorf("error writing XML declaration:", err)
	}
	return nil
}
