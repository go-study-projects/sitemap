module gitlab.com/go-study-projects/sitemap

go 1.21

require (
	gitlab.com/go-study-projects/link-parser v0.0.0-20230926142339-0c1998e7fb95 // indirect
	golang.org/x/net v0.15.0 // indirect
)
